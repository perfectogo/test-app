package config

type Config struct {
	HTTPPort string
	DBConfig DBConfig
}

type DBConfig struct {
	User     string
	Password string
	Host     string
	Port     string
	Database string
}

func Load() *Config {
	return &Config{
		HTTPPort: ":8080",
		DBConfig: DBConfig{
			User:     "postgres",
			Password: "postgres",
			Host:     "localhost",
			Port:     "5432",
			Database: "postgres",
		},
	}
}
