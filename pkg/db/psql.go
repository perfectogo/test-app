package db

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/perfectogo/test-app/config"
)

func ConnectToDB(config config.Config) (*pgxpool.Pool, error) {

	// Set up the connection pool configuration
	connString := fmt.Sprintf("postgresql://%s:%s@%s:%s/%s",
		config.DBConfig.User,
		config.DBConfig.Password,
		config.DBConfig.Host,
		config.DBConfig.Port,
		config.DBConfig.Database,
	)

	conf, err := pgxpool.ParseConfig(connString)
	if err != nil {
		return nil, err
	}

	// Create the connection pool
	pool, err := pgxpool.ConnectConfig(context.Background(), conf)
	if err != nil {
		return nil, err
	}

	return pool, nil
}
