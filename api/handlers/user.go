package handlers

import (
	"fmt"
	"net/http"
)

func (h *Handler) User(w http.ResponseWriter, r *http.Request) {

	if r.Method == http.MethodPost {
		fmt.Println(r.URL.EscapedPath())
		PostUser(w, r)
		fmt.Printf("method: %v", r.Method)
	} else if r.Method == http.MethodGet {
		GetUser(w, r)
		//fmt.Printf("method: %v, response status: %v", r.Method, r.Response.Status)
	} else if r.Method == http.MethodPut {
		UpdateUser(w, r)
		//fmt.Printf("method: %v, response status: %v", r.Method, r.Response.Status)
	} else if r.Method == http.MethodDelete {
		DeleteUser(w, r)
		//	fmt.Printf("method: %v, response status: %v", r.Method, r.Response.Status)
	}
}

func PostUser(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Post User"))
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Get User"))
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Update User"))
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Delete User"))
}
