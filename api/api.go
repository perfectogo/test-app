package api

import (
	"net/http"

	"gitlab.com/perfectogo/test-app/api/handlers"
	"gitlab.com/perfectogo/test-app/config"
)

type RouterOptions struct {
	Config *config.Config
}

func New(optind *RouterOptions) func(string, http.Handler) error {

	handler := handlers.NewHandler(handlers.HandlerOptions{})

	http.HandleFunc("/user", handler.User)

	return http.ListenAndServe
}

func Api(w http.ResponseWriter, r *http.Request) {

}
