package main

import (
	"fmt"

	"gitlab.com/perfectogo/test-app/api"
	"gitlab.com/perfectogo/test-app/config"
)

func main() {
	config := config.Load()

	run := api.New(
		&api.RouterOptions{Config: config},
	)

	fmt.Printf("Server started. Listening on port %v\n", config.HTTPPort)
	run(config.HTTPPort, nil)
}
